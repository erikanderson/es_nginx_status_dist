#!/usr/bin/env python
import time
from time import strftime
from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
import re
import pprint

es = Elasticsearch()

nginx_log_path = "/usr/local/var/log/nginx/access.log"

#https://pypi.python.org/pypi/ngxtop/0.0.2
with open(nginx_log_path) as f:
    f.seek(0, 2)  # seek to eof
    while True:
        line = f.readline()
        if not line:
            time.sleep(0.1)  # sleep briefly before trying again
            continue
        match_status = re.search(r'HTTP[^"]*" (\d+)\s', line).group(1)
        match_date = re.search(r'\[(.*)\]', line).group(1)
        date_obj = datetime.strptime(match_date, "%d/%b/%Y:%H:%M:%S %z")
        print(date_obj)
        #https://elasticsearch-py.readthedocs.io/en/master/
        millis = int(round(time.time() * 1000))
        doc = {
            'text': line,
            'status': match_status,
            'millis': millis,
            'timestamp': date_obj,
        }
        #Create document
        res = es.index(index="nginx", doc_type='log', body=doc)
        #Search for all documents in the last hour in nginx index
        one_hour_ago = millis - 3600000
        s = Search(using=es, index="nginx") \
          .filter('range', millis={'gte': one_hour_ago, 'lte': millis})
        s.aggs.bucket('status_distribution', 'terms', field='status.keyword')
        response = s.execute()
        for hit in response['aggregations']['status_distribution']['buckets']:
          print(f"{hit['key']}: {hit['doc_count']}")
        print('********')

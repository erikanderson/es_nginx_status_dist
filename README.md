###Local Setup (OSX):

```
sudo easy_install pip
sudo pip install virtualenv
virtualenv -p /usr/local/bin/python3 py3env
source py3env/bin/activate
sudo pip install elasticsearch elasticsearch_dsl
```

Install Docker for OSX: https://docs.docker.com/docker-for-mac/install/

```
docker-compose up
```

Run ```python aggregateNginxLogs.py```

Access localhost:8080
